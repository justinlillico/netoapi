from netoapi.api import NetoApi
import requests
from json import loads

class PaymentApi(NetoApi):

    def __init__(self, customer_url, api_key):
        super().__init__(customer_url, api_key)

    def add_payment(self, payment):
        ep = "AddPayment"
        
        if "Payment" in payment:
            return self.api_request(payment, ep)

        if isinstance(payment, list):
            payload = {
                "Payment": payment
            }
        else:
            payload = {
                "Payment": [payment]
            }

        return self.api_request(payload, ep)