from netoapi.api import NetoApi
import requests
from json import loads

class ProductApi(NetoApi):

    def __init__(self, customer_url, api_key):
        super().__init__(customer_url, api_key)

    def get_product_json(self, _type):
        
        if _type == "item":
            r = {
                "Item" : {

                }

            }
        if _type == "filter":
            r = {
                "Filter" : {

                },
                "OutputSelector" : ""

            }
        return r

    def update_products(self, items):
        # pass in a list of dicts or just a single dict.
        ep = "UpdateItem"
        payload = {}
        if isinstance(items, list):
            payload["Item"] = items
        else:
            payload["Item"] = [items]
        
        return self.api_request(payload, ep)

    def add_product(self, product):
        ep = "AddItem"
        payload = self.get_product_json("item")

        payload["Item"] = product

        return self.api_request(payload, ep)

    def get_product(self, content):
        ep = "GetItem"

        if not isinstance(content, list):
            content = [content]

        payload = {
            "Filter": content
        }

        return self.api_request(payload, ep)  

    def get_all_products(self, output_selectors=[]):
        return self.api_request({"DateCreatedFrom" : 1900, "OutputSelector" : output_selectors}, "GetItem")      

def puneeth_trial():
    return ProductApi("puneethtrialsto.neto.com.au", "6efATksDY1GjZQ3Gjo3cTVcQ7YozZlEN")      


if __name__ == "__main__":
    a = ProductApi("brawndo.staging-aws.neto.net.au", "mAGuEfyATNoLj480eTxmAZhNsBPOUZtf")
    print(a.sku_in_db("NN40-1004-0056_as"))