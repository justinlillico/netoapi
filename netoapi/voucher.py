from netoapi.api import NetoApi
import requests
from json import loads

class VoucherApi(NetoApi):

    def __init__(self, customer_url, api_key):
        super().__init__(customer_url, api_key)

    def add_voucher(self, content):
        ep = "AddVoucher"
        payload = {
            "Voucher": [content]
        }

        return self.api_request(payload, ep)

    def update_voucher(self, content):
        ep = "UpdateVoucher"
        payload = {
            "Voucher": [content]
        }

        return self.api_request(payload, ep)

    def get_voucher(self, content):
        ep = "GetVoucher"
        payload = {
            "Voucher": [content]
        }

        return self.api_request(payload, ep)

