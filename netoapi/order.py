from netoapi.api import NetoApi
import requests
from json import loads

class OrderApi(NetoApi):

    def __init__(self, customer_url, api_key):
        super().__init__(customer_url, api_key)

    def add_order(self, order):
        ep = "AddOrder"
        if isinstance(order, list):
            payload = {
                "Order": order
            }
        else:
            payload = {
                "Order": [order]
            }

        return self.api_request(payload, ep)