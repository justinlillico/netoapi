from netoapi.api import NetoApi
import requests
from json import loads

class CustomerApi(NetoApi):

    def __init__(self, customer_url, api_key):
        super().__init__(customer_url, api_key)

    def get_customer(self, content):
        ep = "GetCustomer"
        payload = {
            "Filter": [content]
        }

        return self.api_request(payload, ep)

    def update_customer(self, content):
        ep = "UpdateCustomer"
        if isinstance(content, list):
            payload = { "Customer" : content }
        else:
            payload = {
                "Customer": [content]
            }

        return self.api_request(payload, ep)

    def add_customer(self, content):
        ep = "AddCustomer"
        
        if not isinstance(content, list):
            payload = [content]
            
        payload = {
            "Customer" : content
        }

        return self.api_request(payload, ep)


