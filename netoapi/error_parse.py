
# Example
# {'Item': '', 'CurrentTime': '2020-05-15 02:00:00', 'Ack': 'Warning', 'Messages': {'Warning': {'Message': 'SKU already exists - use UpdateItem 008D7C', 'SeverityCode': 'Warning'}}}


class ApiError():
    pass

class UnknownError(ApiError):
    code = 0
    name = "UNKNOWN"
    desc = "An unknown API error has occured. Please check the output"

    def __init__(self, original_error):
        self.original_error = original_error

class InvalidResponseError(ApiError):
    code = 1
    name = "INVALIDRESPONSE"
    desc = "The response you have provided does not match the format of a Neto response."

    def __init__(self, original_error):
        self.original_error = original_error

class SKUExistsError(ApiError):
    code = 2
    name = "SKUEXISTS"
    desc = "The SKU provided does not exist."

    def __init__(self, original_msg):
        self.original_msg = original_msg
        self.sku = original_msg["Message"].split(" UpdateItem ")[-1]

class CustomerExistsError(ApiError):
    code = 3
    name = "CUSTOMEREXISTS"
    desc = "The Customer exists already"

    def __init__(self, original_msg):
        self.original_msg = original_msg



class ErrorParser():

    warning = "Warning"
    success = "Success"

    def __init__(self):
        self.msg_to_code = {
            "SKU already exists" : SKUExistsError
        }

    # Only use this method if the key is a part key and it is unique to that error.
    def string_in_key(self, string):
        for k in self.msg_to_code.keys():
            if k in string:
                return self.msg_to_code[k]
            return UnknownError

    def identify_error(self, response):
        try:
            ack = response['Ack']
        except KeyError:
            return InvalidResponseError(response)
        r = []
        if ack == self.warning:
            msgs = response["Messages"][self.warning]
            if isinstance(msgs, list):
                for m in msgs:
                    r.append(self.string_in_key(m["Message"])(m))
            else:
                r.append(self.string_in_key(msgs["Message"])(msgs))
            
            if len(r) > 1:
                return r
            else:
                return r[0]

        if ack == self.success:
            return False






