import requests
import json
from requests.exceptions import ReadTimeout
from netoapi.error_parse import ErrorParser

class NetoApi(object):

    def __init__(self, customer_url, api_key):
        self.customer_url = f"https://{customer_url}/do/WS/NetoAPI"
        self.api_key = api_key
        self.headers = {

            "NETOAPI_KEY" : api_key,
            "NETOAPI_ACTION" : "",
            "Accept" : "application/json"

        }

        self.error_parser = ErrorParser()


    def api_request(self, payload, endpoint, timeout=120, retry=3):
        self.headers["NETOAPI_ACTION"] = endpoint
        attempts = 0
        while attempts < retry:
            try:
                response = requests.post(self.customer_url, json=payload, headers=self.headers, timeout=timeout)
                attempts = retry # Break the loop.
            except Exception as e:
                if attempts == 2:
                    raise e
                attempts += 1
        return response.json()

    def identify_error(self, response):
        return self.error_parser.identify_error(response)
  