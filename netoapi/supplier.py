from netoapi.api import NetoApi
import requests
from json import loads

class SupplierApi(NetoApi):

    def __init__(self, customer_url, api_key):
        super().__init__(customer_url, api_key)
        self.add_supplier_reqs = ["SupplierCompany", "SupplierID", "SupplierReference", "LeadTime1", "LeadTime2"]

    def get_product_json(self):
        r = {
            "Supplier" : {

            }

        }
        return r

    def add_value_if_not_present(self, _dict, key, value=0):
         if key not in _dict.keys():
            _dict[key] = value

    def add_supplier(self, suppliers):
        # Simple error checking

        if isinstance(suppliers, list):
            for supplier in suppliers:

                self.add_value_if_not_present(supplier, "LeadTime1")
                self.add_value_if_not_present(supplier, "LeadTime2")
                self.add_value_if_not_present(supplier, "SupplierReference", value = "null")

                for req in self.add_supplier_reqs:
                    if req not in supplier.keys():
                        raise Exception("Must have " + req + " in json request.")
        
        if isinstance(suppliers, dict):
            self.add_value_if_not_present(suppliers, "LeadTime1")
            self.add_value_if_not_present(suppliers, "LeadTime2")
            self.add_value_if_not_present(suppliers, "SupplierReference", value = "null")
            for req in self.add_supplier_reqs:
                if req not in suppliers.keys():
                    raise Exception("Must have " + req + " in json request.")


        # pass in a list of dicts or just a single dict.
        ep = "AddSupplier"

        payload = self.get_product_json()
        payload["Supplier"] = suppliers

        return self.api_request(payload, ep)