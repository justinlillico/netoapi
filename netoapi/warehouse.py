from netoapi.api import NetoApi
import requests
from json import loads

class WarehouseApi(NetoApi):

    def __init__(self, customer_url, api_key):
        super().__init__(customer_url, api_key)


    def add_warehouse(self, content):
        ep = "AddWarehouse"
        payload = {
            "Warehouse": [content]
        }

        return self.api_request(payload, ep)

    def update_warehouse(self, content):
        ep = "UpdateWarehouse"
        payload = {
            "Warehouse": [content]
        }

        return self.api_request(payload, ep)

    def get_warehouse(self, content):
        ep = "GetWarehouse"
        payload = {
            "Filter": [content]
        }

        return self.api_request(payload, ep)

