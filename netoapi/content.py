from netoapi.api import NetoApi
import requests
from json import loads

class ContentApi(NetoApi):

    def __init__(self, customer_url, api_key):
        super().__init__(customer_url, api_key)

    def get_content_json(self):
        r = {
                "Filter" : {
                    "OutputSelector" : ["ContentName", "ContentID"],
                    "ContentName" : ""
                }
            }

        return r

    def add_content(self, content):
        ep = "AddContent"

        if isinstance(content, dict):
            content = [content]
            
        payload = {
            "Content": content
        }

        return self.api_request(payload, ep)

    def update_content(self, content):
        ep = "UpdateContent"
        if isinstance(content, list):
            payload = { "Content" : content }
        else:
            payload = {
                "Content": [content]
            }

        return self.api_request(payload, ep)

    def get_content(self, content):
        ep = "GetContent"
        payload = {
            "Filter": content
        }

        return self.api_request(payload, ep)

    def get_all_content(self, output_selectors=[]):
        return self.api_request({"DateCreatedFrom" : 1900, "OutputSelector" : output_selectors}, "GetContent")      

def puneeth_trial():
    return ContentApi("puneethtrialsto.neto.com.au", "6efATksDY1GjZQ3Gjo3cTVcQ7YozZlEN")  

